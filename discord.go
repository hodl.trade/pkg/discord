package discord

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const (
	MAX_USERNAME_LENGTH = 80
	MAX_MESSAGE_LENGTH  = 2000
	MAX_EMBEDS          = 10
)

type Sender interface {
	MessageSender
	EmbedsSender
}

type MessageSender interface {
	Send(message string) error
}

type EmbedsSender interface {
	SendEmbeds(embeds ...map[string]interface{}) error
}

type sender struct {
	username string
	webhooks []string
}

type Option interface {
	apply(*sender) error
}

func New(options ...Option) (_ Sender, err error) {
	t := &sender{}
	for _, o := range options {
		if err = o.apply(t); err != nil {
			return
		}
	}
	return t, nil
}

func NewFromEnv(options ...Option) (_ Sender, err error) {
	for _, s := range os.Environ() {
		k, v := "", ""
		for j := 0; j < len(s); j++ {
			if s[j] == '=' {
				k = s[:j]
				v = s[j+1:]
				break
			}
		}
		if k == "" {
			continue
		}

		if k == "DISCORD_WEBHOOK" {
			options = append(options, Webhook(v))
		} else if strings.HasPrefix("DISCORD_WEBHOOK_", v) {
			options = append(options, Webhook(v))
		}
	}

	return New(options...)
}

func Username(value string) Option {
	return optionFn(func(t *sender) error {
		if len(value) > MAX_USERNAME_LENGTH {
			return fmt.Errorf("Username longer than %d characters", MAX_USERNAME_LENGTH)
		}
		t.username = value
		return nil
	})
}

func Webhook(value string) Option {
	return optionFn(func(t *sender) error {
		t.webhooks = append(t.webhooks, value)
		return nil
	})
}

type optionFn func(t *sender) error

func (f optionFn) apply(t *sender) error {
	return f(t)
}

// sender

func (t *sender) Send(message string) (err error) {
	return t.sendPayload(map[string]interface{}{
		"content": message,
	})
}

func (t *sender) SendEmbeds(embeds ...map[string]interface{}) (err error) {
	if len(embeds) > MAX_EMBEDS {
		return fmt.Errorf("only %d embeds are allowed", MAX_EMBEDS)
	}

	return t.sendPayload(map[string]interface{}{
		"embeds": embeds,
	})
}

func (t *sender) sendPayload(payload map[string]interface{}) (err error) {
	if _, ok := payload["username"]; !ok && t.username != "" {
		payload["username"] = t.username
	}

	var b []byte
	if b, err = json.Marshal(payload); err != nil {
		return
	}

	for _, d := range t.webhooks {
		if err = post(d, "application/json", b); err != nil {
			return
		}
	}

	return
}

func post(url string, contentType string, blob []byte) (err error) {
	var res *http.Response
	if res, err = http.Post(url, contentType, bytes.NewReader(blob)); err != nil {
		return
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusNoContent:
		return
	case http.StatusBadRequest:
		response := map[string][]string{}
		if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
			return err
		}

		b := strings.Builder{}
		first := true
		for f, errs := range response {
			for _, e := range errs {
				if !first {
					b.WriteString("\n")
				} else {
					first = false
				}
				b.WriteString(fmt.Sprintf("%s: %s", f, e))
			}
		}

		return errors.New(b.String())
	default:
		e := &UnexpectedStatusCode{
			code: res.StatusCode,
		}
		if e.body, err = ioutil.ReadAll(res.Body); err != nil {
			return
		}
		return e
	}
}

type UnexpectedStatusCode struct {
	code int
	body []byte
}

func (e *UnexpectedStatusCode) Error() string {
	return fmt.Sprintf("unexpected status code: %d", e.code)
}

func (e *UnexpectedStatusCode) StatusCode() int {
	return e.code
}

func (e *UnexpectedStatusCode) Body() []byte {
	return e.body[:]
}
